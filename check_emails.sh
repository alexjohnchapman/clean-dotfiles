#!/bin/env bash

echo "$MAILX_PW" | mailx -A gmail | tee ~/latest-emails.txt

EMAILS=$(rg ">?U" ~/latest-emails.txt)

notify-send "$EMAILS"

if rg ">?U" "$EMAILS"; then
    notify-send "Open your emails"
else
    notify-send "No new emails"
fi
