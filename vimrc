set guifont=Monospace\ 12

colo elflord 
set signcolumn=number
set number
set bg=dark
set visualbell
syntax on

set tags=_tag_file_

set foldmethod=indent
autocmd BufWinEnter * normal! zR

set hidden
set t_Co=256
set tabstop=4
set shiftwidth=4
set expandtab

autocmd FileType go call SetGoOptions()

function SetGoOptions()
   set tabstop=8
   set shiftwidth=8
   set noexpandtab
endfunction

autocmd FileType java call SetJavaOptions()

function SetJavaOptions()
   set tabstop=4
   set shiftwidth=4
   set expandtab
endfunction

autocmd FileType yaml call SetYamlOptions()

function SetYamlOptions()
    set tabstop=2
    set shiftwidth=2
    set expandtab
endfunction

set modelines=0
set autoindent
set showmode
set wildmenu
set backspace=indent,eol,start
set ruler
set nocursorline
set nocompatible
set noswapfile

set ignorecase
set smartcase
set incsearch
set showmatch
set hlsearch

set splitbelow
set splitright

" Set leader to be ,
let mapleader = ","

" Clear highlighted text with ,<space>
nnoremap <leader><space> :noh<CR>
nnoremap <tab> %
vnoremap <tab> %

" Select the word under the cursor for easy find/replace
nnoremap sw :%s/<C-R><C-W>//g<left><left>

" Add a curly bracket block with a{
nnoremap a{ i { }<C-o>h<CR><CR><C-o>k<Tab>
" Start editing the vimrc file with ,e
nnoremap <leader>e :e ~/.vimrc<CR>
" Source the vimrc fild with ,0
nnoremap <leader>0 :so ~/.vimrc<CR>
" Close a buffer with ,q
nnoremap <leader>q :bd<CR>
" Close a buffer without closing a window with ,Q
nnoremap <leader>Q :bn<bar>:bd#<CR>

" Unimpaired-esque switching between tabs
nnoremap [t :tabp<CR>
nnoremap ]t :tabN<CR>

" Map F1 to escape, as I never want to open up the help page when I'm trying
" to exit insert mode
nnoremap <F1> <Esc>
inoremap <F1> <Esc>
vnoremap <F1> <Esc>

" VIM-PLUG "
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.vimrc
endif

call plug#begin()
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'janko/vim-test'
Plug 'bling/vim-bufferline'
Plug 'posva/vim-vue'
Plug 'rust-lang/rust.vim'
" Requires fzf to be installed
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'pantharshit00/vim-prisma'
Plug 'jiangmiao/auto-pairs'
call plug#end()

" Plugin specific config "

" coc.nvim
let g:coc_global_extensions = [
    \ 'coc-tsserver',
    \ 'coc-eslint',
    \ 'coc-prettier',
    \ 'coc-vetur',
    \ 'coc-json',
    \ 'coc-rust-analyzer',
    \ 'coc-markdownlint',
    \ 'coc-go',
    \ 'coc-prisma',
\ ]

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> gh <Plug>(coc-action-doHover)

" Symbol renaming
nmap <silent> rn <Plug>(coc-rename)

" Fix current line
nmap <leader>a <Plug>(coc-codeaction)
nmap <leader>f <Plug>(coc-fix-current)

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Trigger completion with ctrl-space
inoremap <silent><expr> <c-space> coc#refresh()

" Vim Test
let test#strategy = 'vimterminal'

" FZF
nnoremap <C-p> :GFiles<CR>
let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --glob "!.git/*"'

nnoremap <C-f> :Rg <C-R><C-W><CR>
