#! /bin/zsh
set -eo pipefail
WORK_NOTE_PATH="$HOME/.work_notes.md"
printf "\n---\n\n## $(date -u +"%Y-%m-%dT%H:%M:%SZ")\n\n" >>"$WORK_NOTE_PATH"
lvim + "$WORK_NOTE_PATH"
