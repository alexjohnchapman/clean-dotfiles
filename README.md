# How to install new dev environment

Gnome is a bit of an effort to switch from bash to zsh.

* Install zsh
* Remove .zshrc
* `cd ~`
* `git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh`
* `git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting`
* `git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm`
* `git clone "this-repo"`
* `ln -s ~/clean-dotfiles/zshrc ~/.zshrc`
* `ln -s ~/clean-dotfiles/vimrc ~/.vimrc`
* `ln -s ~/clean-dotfiles/tmux.conf ~/.tmux.conf`
* `ln -s ~/clean-dotfiles/coc-settings.json ~/.vim/coc-settings.json`
* `mkdir ~/.bin`
* `ln -s ~/clean-dotfiles/quick-get.py ~/.bin/qget`
* `ln -s ~/clean-dotfiles/gitignore ~/.gitignore`
* `git config --global core.excludesFile ~/.gitignore`

## Extras

tmux, gvim, rg, fzf, bat, lunarvim

For tmux-yank to work, install xsel (tested on Fedora).
