ZSH=~/.oh-my-zsh
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="gallois"

COMPLETION_WAITING_DOTS="true"

plugins=(
    git
    safe-paste
    history-substring-search
    zsh-syntax-highlighting
    wd
    colored-man-pages
    tmux
    web-search
)

source $ZSH/oh-my-zsh.sh

bindkey "^[[A" history-substring-search-up
bindkey "^[[B" history-substring-search-down

bindkey -M vicmd 'l' history-substring-search-up
bindkey -M vicmd 'k' history-substring-search-down

# User configuration
export WASMTIME_HOME="$HOME/.wasmtime"
export PATH=~/.npm-global/bin:~/.local/bin:~/.cargo/bin:~/.bin:~/go/bin:$WASMTIME_HOME/bin:$PATH
export ZSH_TMUX_AUTOSTART=true

# Configures less to write to stdout if the text can be viewed without scrolling
export LESS=-FRX

# Aliases
alias gs="git status"
alias gd="git diff"
alias gpo='git push -u origin $(git branch --show-current)'
alias gentags='ctags -f _tagfile_ -R'

function wdf() {
    wd $(wd list | awk '(NR>1) {print $1}' | fzf)
}

function prettier() {
    npx prettier
}

export EDITOR=~/.local/bin/lvim

# - Note taking
alias note="~/clean-dotfiles/note.sh"
alias vnote="$EDITOR \"+normal G\" ~/.work_notes.md"

if [ -e /home/alex/.nix-profile/etc/profile.d/nix.sh ]; then . /home/alex/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

if [ -e ~/.env ]; then . ~/.env; fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

source ~/.ghcup/env
if [ -e /home/alichapman/.nix-profile/etc/profile.d/nix.sh ]; then . /home/alichapman/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
eval "$(direnv hook zsh)"

fpath+=~/clean-dotfiles/functions
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
eval "$(direnv hook zsh)"

# pnpm
export PNPM_HOME="/home/alichapman/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"
# pnpm end