#!/usr/bin/env python
import subprocess
import sys
import os

url = sys.argv[1]
data = url.split("https://github.com")[1].split("/")
organisation = data[1]
repo = data[2]
gh_path = organisation + "/" + repo
local_path = "/home/alichapman/projects/" + repo

if not os.path.exists(local_path):
    print("Repo hasn't been cloned yet")
    subprocess.run(["gh", "repo", "clone", gh_path, local_path])

os.chdir(local_path)
file = "/".join(data[5:])
subprocess.run(["lvim", file])
exit(0)
